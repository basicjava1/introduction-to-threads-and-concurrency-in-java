package exercise2;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Taskk extends Thread{
    private int number;

    public Taskk(int number){
        this.number = number;
    }

    public void run(){
        System.out.println("\nTask1 " + number + " Started");

        for(int i = number*100;i<number*100+99;i++)
            System.out.println(i+" ");
        System.out.println("\nTask1 " + number +" Done");
    }
}
public class Thread3{
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new Taskk(1));
        executorService.execute(new Taskk(2));
        executorService.execute(new Taskk(3));
        executorService.execute(new Taskk(4));

        executorService.shutdown();
    }
}
