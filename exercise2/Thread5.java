package exercise2;

import exercise2.CallableTask;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Thread5 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

         List<CallableTask> tasks =  List.of(new CallableTask("Sunrise"),
                new CallableTask("Sunset"));

//        List<Future<String>> results = executorService
//                .invokeAll(tasks);

        String result = executorService.invokeAny(tasks);

        System.out.println(result);
        executorService.shutdown();

    }
}
