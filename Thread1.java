// extending thread
// implement Runnable
class Task1 extends Thread{
    public void run(){
        System.out.println("Task1 Started");
        for(int i = 0;i<100;i++)
            System.out.println(i);
        System.out.println("Task1 Done");
    }
}
class Task2 extends Thread{
    public void run(){
        System.out.println("Task2 Started");
        for(int i = 100;i<200;i++)
            System.out.println(i);
        System.out.println("Task2 Done");
    }
}

public class Thread1 {
    public static void main(String[] args) {
        Task1 thread0 = new Task1();
        thread0.start();
        Task2 thread2 = new Task2();
        Thread task2Thread = new Thread(thread2);
        task2Thread.start();


        for(int i = 200;i<300;i++)
            System.out.println(i);
    }
}
